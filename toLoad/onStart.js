const Discord = require("discord.js");
const moment = require("moment");
const title = require("console-title");
const colors = require("colors");

const client = new Discord.Client();

const config = require("../config.json");
const featureLoader = require("./featureLoader.js");
const commandLoader = require("./commandLoader.js");

title("FRN | Loading...");
console.log("Loading...".green);
client.login(config.token).catch(error => {
    if(error) {
        console.log("Error when trying to load bot. Error: " + `${error}`.red);
    };
});

client.on("ready", () => {
    console.clear();
    title("FRN | Logged");
    commandLoader(client);
    featureLoader(client);
    console.log("Client loaded successfully.".green);
});
