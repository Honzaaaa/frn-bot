const Discord = require("discord.js")
const moment = require("moment")
const { prefix } = require('../config.json')

const validatePermissions = (permissions) => {
  const validPermissions = [
    'CREATE_INSTANT_INVITE',
    'KICK_MEMBERS',
    'BAN_MEMBERS',
    'ADMINISTRATOR',
    'MANAGE_CHANNELS',
    'MANAGE_GUILD',
    'ADD_REACTIONS',
    'VIEW_AUDIT_LOG',
    'PRIORITY_SPEAKER',
    'STREAM',
    'VIEW_CHANNEL',
    'SEND_MESSAGES',
    'SEND_TTS_MESSAGES',
    'MANAGE_MESSAGES',
    'EMBED_LINKS',
    'ATTACH_FILES',
    'READ_MESSAGE_HISTORY',
    'MENTION_EVERYONE',
    'USE_EXTERNAL_EMOJIS',
    'VIEW_GUILD_INSIGHTS',
    'CONNECT',
    'SPEAK',
    'MUTE_MEMBERS',
    'DEAFEN_MEMBERS',
    'MOVE_MEMBERS',
    'USE_VAD',
    'CHANGE_NICKNAME', 
    'MANAGE_NICKNAMES',
    'MANAGE_ROLES',
    'MANAGE_WEBHOOKS',
    'MANAGE_EMOJIS',
  ]

  for (const permission of permissions) {
    if (!validPermissions.includes(permission)) {
      throw new Error(`Unknown permission "${permission}"`)
    }
  }
}

module.exports = (client, commandOptions) => {
  let {
    commands,
    expectedArgs = '',
    minArgs = 0,
    maxArgs = null,
    cooldown = -1,
    permissions = [],
    callback,
  } = commandOptions

let recentlyRan = []

  if (typeof commands === 'string') {
    commands = [commands]
  }

    console.log(`[LOG] Loaded command ${commands[0]}.js`);

  if (permissions.length) {
    if (typeof permissions === 'string') {
      permissions = [permissions]
    }

    validatePermissions(permissions)
  }

  client.on('message', (message) => {
    const { member, content, guild } = message

    for (const alias of commands) {
      const command = `${prefix}${alias.toLowerCase()}`

      if (
        content.toLowerCase().startsWith(`${command} `) ||
        content.toLowerCase() === command
      ) {

        for (const permission of permissions) {
          if (!member.hasPermission(permission)) {
            let embed = new Discord.MessageEmbed()
            embed.setTitle("You don't have permissions.")
            embed.setDescription(`You must have permission called` + " `" + `${permission}` + "` to execute this command.")
            embed.setColor("#e403fe")
            embed.setFooter(`Command executed by ${message.author.tag} at ${moment(message.createdTimestamp).format("LL LTS")}`, message.author.displayAvatarURL({ dynamic: true }))
            message.channel.send(embed).catch(error => {
                if(error) {
                    console.log(`[LOG] Error sending "You don't have permissions." embed. Reason: ${error}`)
                }
            })
            return
          }
        }

        let cooldownString = `${guild.id}-${member.id}-${commands[0]}`

        if (cooldown > 0 && recentlyRan.includes(cooldownString)) {
            let embed = new Discord.MessageEmbed()
            embed.setTitle("You are in cooldown.")
            embed.setDescription(`Please wait` + " `" + `${cooldown} seconds` + "` before executing this command again.")
            embed.setColor("#e403fe")
            embed.setFooter(`Command executed by ${message.author.tag} at ${moment(message.createdTimestamp).format("LL LTS")}`, message.author.displayAvatarURL({ dynamic: true }))
            message.channel.send(embed).catch(error => {
                if(error) {
                    console.log(`[LOG] Error sending "You are in cooldown" embed. Reason: ${error}`)
                }
            })
          return
        }

        const arguments = content.split(/[ ]+/)

        arguments.shift()

        if( 
          arguments.length < minArgs ||
          (maxArgs !== null && arguments.length > maxArgs)
        ) {
            let embed = new Discord.MessageEmbed()
            embed.setTitle("Wrong command usage.")
            embed.setDescription(`Use ${prefix}${alias} ` + "`" + `${expectedArgs}` + "`.")
            embed.setColor("#e403fe")
            embed.setFooter(`Command executed by ${message.author.tag} at ${moment(message.createdTimestamp).format("LL LTS")}`, message.author.displayAvatarURL({ dynamic: true }))
            message.channel.send(embed).catch(error => {
                if(error) {
                    console.log(`[LOG] Error sending "Wrong command usage" embed. Reason: ${error}`)
                }
            })
          return
        }
        if (cooldown > 0) {
            if(message.member.hasPermission("ADMINISTRATOR") || message.member.roles.cache.has("777631498122559498")) {
                console.log(`[LOG] Set cooldown to 0 for ${message.author.tag}. Reason: Staff Member Cooldown Bypass.`)
                recentlyRan.push(cooldownString)
                  recentlyRan = recentlyRan.filter((string) => {
                    return string !== cooldownString
                  })
                let embed = new Discord.MessageEmbed()
                embed.setTitle("You have bypassed cooldown.")
                embed.setDescription("Since you are in FRN A-Team, you don't have to wait before executing command again. Congratulations! Enjoy this function.")
                embed.setColor("#e403fe")
                embed.setFooter(`Command executed by ${message.author.tag} at ${moment(message.createdTimestamp).format("LL LTS")}`, message.author.displayAvatarURL({ dynamic: true }))
                message.author.send(embed).catch(error => {
                    if(error) {
                        console.log(`[LOG] Error sending "You have bypassed cooldown" embed. Reason: ${error}`)
                    }
                })
            } else {
                recentlyRan.push(cooldownString)
                console.log(`[LOG] Set cooldown to ${cooldown} for ${message.author.tag}. Reason: Spam protection.`)
                setTimeout(() => {
                  recentlyRan = recentlyRan.filter((string) => {
                    return string !== cooldownString
                  })
                }, 1000 * cooldown)
            }
          }

        callback(message, arguments, arguments.join(' '), client)
            console.log(`[LOG] Runned command ${alias} by ${message.author.tag} at ${moment(message.createdTimestamp).format("LL LTS")}`)
            return
      }
    }
  })
}
