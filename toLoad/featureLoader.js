const Discord = require("discord.js");
const title = require("console-title");
const colors = require("colors");

const config = require("../config.json");

module.exports = (client) => {
    client.on("disconnect", () => {
        console.log("Bot has disconnected. Relogging in.".red);
        client.login(config.token).catch(error => {
            if(error => {
                console.log("Error when loading bot. Error: " + `${error}`.red);
            });
        });
    });
    client.on("rateLimit", () => {
        console.log("Bot is rate limited. Freezing to 5 seconds.");
        setTimeout(() => {}, 5000);
    });
};
