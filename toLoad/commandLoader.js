const path = require('path')
const fs = require('fs')

module.exports = (client) => {
  const command = []
  const readCommand = (dir) => {
    const files = fs.readdirSync(path.join(__dirname, dir))
    for (const file of files) {
      const st = fs.lstatSync(path.join(__dirname, dir, file))
      if (st.isDirectory()) {
        readCommand(path.join(dir, file))
      } else if (file !== "cmd-handler.js" && file !== 'cmd-loader.js') {
        const option = require(path.join(__dirname, dir, file))
        cmds.push(option)
        if (client) {
          handerPath(client, option)
        }
      }
    }
  }
  readCommand('.')
  return command
}
