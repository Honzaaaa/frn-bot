const Discord = require("discord.js")
const moment = require("moment")

module.exports = {
    commands: ['add', 'addition'],
    expectedArgs: '<num1> <num2>',
    minArgs: 2,
    maxArgs: 2,
    cooldown: 1,
    callback: (message, arguments, text) => {
    
    },
    permissions: '',
    requiredRoles: [],
  }
